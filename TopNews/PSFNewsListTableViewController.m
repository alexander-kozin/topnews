//
//  PSFNewsListTableViewController.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <SVProgressHUD/SVProgressHUD.h>
#import "PSFNewsListTableViewController.h"
#import "PSFNewsDetailsViewController.h"
#import "PSFNewsTableViewCell.h"
#import "PSFNewsListViewModel.h"
#import "PSFNewsDetailsViewModel.h"
#import "PSFNewsModel.h"

@interface PSFNewsListTableViewController () {
    PSFNewsListViewModel *viewModel;
}

@end

@implementation PSFNewsListTableViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        viewModel = [PSFNewsListViewModel new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self);
    [[RACObserve(viewModel, model) ignore:nil] subscribeNext:^(id x) {
        @strongify(self);
        [self.tableView reloadData];
    }];
    
    viewModel.active = YES;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [[RACObserve(viewModel, loading) ignore:nil] subscribeNext:^(NSNumber *loading) {
        if ( loading.boolValue ) {
            [SVProgressHUD show];
        } else {
            [SVProgressHUD dismiss];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return viewModel.model.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *kCellId = @"NewsTableViewCell";
    PSFNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId forIndexPath:indexPath];
    
    PSFNewsModel *newsModel = viewModel.model[indexPath.row];
    cell.titleLabel.text = newsModel.title;
    cell.creationDateLabel.text = [viewModel creationDateStringForModelAtIndex:indexPath.row];
    cell.newsModel = newsModel;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedRowIndexPath = [self.tableView indexPathForSelectedRow];
    if ( selectedRowIndexPath ) {
        PSFNewsModel *newsModel = viewModel.model[selectedRowIndexPath.row];
        PSFNewsDetailsViewModel *detailsViewModel = [[PSFNewsDetailsViewModel alloc] initWithModel:newsModel];
        PSFNewsDetailsViewController *detailsVC = [segue destinationViewController];
        detailsVC.viewModel = detailsViewModel;
    } else {
        NSLog(@"Selected row is missing");
        abort();
    }
}

@end
