//
//  PSFNewsTitleCollectionViewCell.h
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PSFFavoriteControl;
@class PSFNewsModel;

@interface PSFNewsTitleCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *creationDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, getter=isFavorite) BOOL favorite;
@property (nonatomic) PSFNewsModel *newsModel;

@end
