//
//  PSFFavoriteNews.m
//  TopNews
//
//  Created by Alexander Kozin on 08.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFFavoriteNews.h"
#import "PSFNewsModel.h"
#import "PSFSettings.h"

@interface PSFFavoriteNews () {
    NSMutableSet<PSFNewsModel *> *newsSet;
    PSFSettings *settings;
}

@property (readonly, nonatomic) NSString *storagePath;
@property (readonly, nonatomic) NSMutableSet *savedNewsData;

@end

@implementation PSFFavoriteNews

static NSString * const kNewsSetKey = @"newsSet";

+ (instancetype)sharedFavoriteNews {
    static PSFFavoriteNews *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[PSFFavoriteNews alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    if ( self = [super init] ) {
        settings = [PSFSettings sharedSettings];
        newsSet = self.savedNewsData;
        if ( !newsSet ) {
            newsSet = [NSMutableSet new];
        }
    }
    return self;
}

/**
 Получение списка избранных новостей, отсортированных в порядке их создания 
 от самых новых к самым старым.

 */
- (NSArray *)newsList {
    NSSortDescriptor *createdDateSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"created" ascending:NO];
    return [newsSet sortedArrayUsingDescriptors:@[createdDateSortDescriptor]];
}

- (void)addToFavorites:(PSFNewsModel *)newsModel {
    if ( ![newsSet containsObject:newsModel] ) {
        [self willChangeValueForKey:@"newsList"];
        [newsSet addObject:newsModel];
        [self didChangeValueForKey:@"newsList"];
        newsModel.favorite = YES;
        [self saveToStorage];
    }
}

- (void)removeFromFavorites:(PSFNewsModel *)newsModel {
    if ( [newsSet containsObject:newsModel] ) {
        [self willChangeValueForKey:@"newsList"];
        [newsSet removeObject:newsModel];
        [self didChangeValueForKey:@"newsList"];
        newsModel.favorite = NO;
        [self saveToStorage];
    }
}

- (BOOL)containsModel:(PSFNewsModel *)newsModel {
    return [newsSet containsObject:newsModel];
}

- (void)saveToStorage {
    [NSKeyedArchiver archiveRootObject:newsSet toFile:self.storagePath];
}

- (NSMutableSet *)savedNewsData {
    return [[NSKeyedUnarchiver unarchiveObjectWithFile:self.storagePath] mutableCopy];
}

/**
 Путь до хранилища с избранными новостями

 */
- (NSString *)storagePath {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *cachesDirectories = [fm URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *cacheDirectoryURL = [cachesDirectories firstObject];
    NSURL *storagePathURL = [cacheDirectoryURL URLByAppendingPathComponent:settings.favoriteNewsStorageName];
    return storagePathURL.path;
}

@end
