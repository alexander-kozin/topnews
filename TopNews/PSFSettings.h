//
//  PSFSettings.h
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSFSettings : NSObject

@property (readonly, nonatomic) NSURL *newsURL;
@property (readonly, nonatomic) NSString *favoriteNewsStorageName;

+ (instancetype)sharedSettings;

@end
