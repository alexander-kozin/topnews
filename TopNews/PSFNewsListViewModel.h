//
//  PSFNewsListViewModel.h
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PSFNewsModel;

@interface PSFNewsListViewModel : RVMViewModel

@property (readonly, nonatomic) NSArray<PSFNewsModel *> *model;
@property (readonly, readonly, getter=isLoading) BOOL loading;

- (NSString *)creationDateStringForModelAtIndex:(NSInteger)index;

@end
