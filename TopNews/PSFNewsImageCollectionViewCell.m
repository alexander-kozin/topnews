//
//  PSFNewsImageCollectionViewCell.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsImageCollectionViewCell.h"
#import "PSFNewsDownloader.h"

@interface PSFNewsImageCollectionViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *newsImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *imageLoadingActivityIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;

@end

@implementation PSFNewsImageCollectionViewCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        [[RACObserve(self, newsImageURL) ignore:nil] subscribeNext:^(NSURL *url) {
            [[PSFNewsDownloader fetchNewsImage:url] subscribeNext:^(NSData *data) {
                self.newsImageView.image = [UIImage imageWithData:data];
                [self.imageLoadingActivityIndicator stopAnimating];
            }];
        }];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.widthConstraint.constant = UIScreen.mainScreen.bounds.size.width;
    self.heightConstraint.constant = 200;
}

@end
