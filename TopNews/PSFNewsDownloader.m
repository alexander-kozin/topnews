//
//  PSFNewsDownloader.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsDownloader.h"
#import "PSFSettings.h"
#import "PSFNewsModel.h"

@interface PSFNewsDownloader () {
    PSFSettings *settings;
}

+ (NSURL *)imageFileURL:(NSURL *)imageNetworkURL;
+ (void)moveFileAtURL:(NSURL *)fromURL toURL:(NSURL *)toURL;
+ (BOOL)jsonNewsDataIsValid:(NSDictionary *)newsDictionary;
+ (void)configureNewsModel:(PSFNewsModel *)model withNewsData:(NSDictionary *)data;
+ (NSUInteger)stringCharCodesSum:(NSString *)aString;

@end

@implementation PSFNewsDownloader

/**
 Загрузка списка новостей
 
 */
+ (RACSignal *)fetchNews {
    NSURL *newsURL = [[PSFSettings sharedSettings] newsURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:newsURL];
    return [[[[NSURLConnection rac_sendAsynchronousRequest:request]
            reduceEach:^id(NSURLResponse *response, NSData *data) {
                return data;
            }] map:^id(NSData *data) {
                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                return [[[[jsonData[@"articles"] rac_sequence] filter:^BOOL(NSDictionary *newsDictionary){
                    return [self jsonNewsDataIsValid:newsDictionary];
                }] map:^id(NSDictionary *newsDictionary) {
                    PSFNewsModel *model = [PSFNewsModel new];
                    [self configureNewsModel:model withNewsData:newsDictionary];
                    return model;
                }] array];
            }] deliverOn:[RACScheduler mainThreadScheduler]];
}

/**
 Загрузка изображения новости

 @param imageURL Ссылка на изображение
 */
+ (RACSignal *)fetchNewsImage:(NSURL *)imageURL {
    NSURL *imageFileURL = [self imageFileURL:imageURL];
    if ( [imageFileURL checkResourceIsReachableAndReturnError:nil] ) {
        return [NSData rac_readContentsOfURL:imageFileURL options:0
                                   scheduler:[RACScheduler mainThreadScheduler]];
    } else {
        RACSignal *signal = [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            NSURLSession  *session = NSURLSession.sharedSession;
            NSURLSessionDownloadTask *downloadTask = [session
                                                      downloadTaskWithURL:imageURL
                                                      completionHandler:^(NSURL *location,
                                                                          NSURLResponse *response,
                                                                          NSError *error) {
                                                          if ( !location ) {
                                                              [subscriber sendError:error];
                                                              return;
                                                          }
                                                          // сохранение загруженного изображения в кэше
                                                          [self moveFileAtURL:location toURL:imageFileURL];
                                                          NSData *data = [NSData dataWithContentsOfURL:imageFileURL];
                                                          [subscriber sendNext:data];
                                                          [subscriber sendCompleted];
                                                      }];
            [downloadTask resume];
            return [RACDisposable disposableWithBlock:^{
                [downloadTask cancel];
            }];
        }] deliverOn:[RACScheduler mainThreadScheduler]];
        
        return signal;
    }
}

+ (void)configureNewsModel:(PSFNewsModel *)model withNewsData:(NSDictionary *)data {
    model.title = data[@"title"];
    model.fullDescription = data[@"description"];
    NSString *imageURLString = data[@"urlToImage"];
    model.imageURL = [NSURL URLWithString:imageURLString];
    
    NSString *newsCreationDateString = data[@"publishedAt"];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZ";
    NSDate *creationDate = [df dateFromString:newsCreationDateString];
    // дата приходит в разных форматах (с микросекундами и без)
    if ( !creationDate ) {
        df.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
        creationDate = [df dateFromString:newsCreationDateString];
    }
    model.created = [df dateFromString:newsCreationDateString];
}

+ (void)moveFileAtURL:(NSURL *)fromURL toURL:(NSURL *)toURL {
    NSFileManager *fm = [NSFileManager defaultManager];
    if ( [toURL checkResourceIsReachableAndReturnError:nil] ) {
        // если файл с таким имененем уже существует, то удаляю его
        [fm removeItemAtURL:toURL error:nil];
    }
    [fm moveItemAtURL:fromURL toURL:toURL error:nil];
}

+ (NSURL *)imageFileURL:(NSURL *)imageNetworkURL {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSArray *directoriesURLs = [fm URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSURL *cachesDirectoryURL = directoriesURLs.firstObject;
    NSUInteger imageURLUniqueId = [self stringCharCodesSum:imageNetworkURL.absoluteString];
    NSString *imageName = [imageNetworkURL.lastPathComponent stringByAppendingFormat:@"_%lu", imageURLUniqueId];
    NSString *imageExtension = imageNetworkURL.pathExtension;
    if ( imageExtension ) {
        imageName = [imageName stringByAppendingFormat:@".%@", imageExtension];
    }
    NSString *cachedImagesDirectoryName = @"newsImages";
    NSURL *cachedImagesDirectoryURL = [cachesDirectoryURL URLByAppendingPathComponent:cachedImagesDirectoryName];
    if ( ![cachedImagesDirectoryURL checkResourceIsReachableAndReturnError:nil] ) {
        [fm createDirectoryAtURL:cachedImagesDirectoryURL withIntermediateDirectories:NO
                      attributes:nil error:nil];
    }
    return [cachedImagesDirectoryURL URLByAppendingPathComponent:imageName];
}

/**
 Проверка, что в информации о новости нет пустых полей.

 @param newsDictionary Словарь с новостью
 @return Статус проверки
 */
+ (BOOL)jsonNewsDataIsValid:(NSDictionary *)newsDictionary {
    __block BOOL dataIsValid = YES;
    [newsDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL *stop) {
        if ( !value ) {
            dataIsValid = NO;
            *stop = YES;
        }
    }];
    return dataIsValid;
}

/**
 Вычисление суммы кодов символов, входящих в строку

 @param aString Строка
 @return Сумма кодов символов
 */
+ (NSUInteger)stringCharCodesSum:(NSString *)aString {
    NSUInteger charCodesSum = 0;
    for ( NSUInteger charNumber = 0; charNumber < aString.length; charNumber++ ) {
        unichar charCode = [aString characterAtIndex:charNumber];
        charCodesSum += charCode;
    }
    return charCodesSum;
}

@end
