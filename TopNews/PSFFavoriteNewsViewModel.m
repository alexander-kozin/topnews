
//
//  PSFFavoriteNewsViewModel.m
//  TopNews
//
//  Created by Alexander Kozin on 08.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFFavoriteNewsViewModel.h"
#import "PSFFavoriteNews.h"
#import "PSFNewsModel.h"

@interface PSFFavoriteNewsViewModel () {
    PSFFavoriteNews *favoriteNews;
}

@end

@implementation PSFFavoriteNewsViewModel

- (instancetype)init {
    if ( self = [super init] ) {
        favoriteNews = [PSFFavoriteNews sharedFavoriteNews];
        RAC(self, favoriteNewsList) = RACObserve(favoriteNews, newsList);
    }
    return self;
}

- (NSString *)creationDateStringForModelAtIndex:(NSInteger)index {
    if ( index < 0 || index > self.favoriteNewsList.count ) {
        return @"";
    }
    PSFNewsModel *newsModel = self.favoriteNewsList[index];
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateStyle = NSDateFormatterNoStyle;
    df.timeStyle = NSDateFormatterShortStyle;
    NSString *creationDateString = [df stringFromDate:newsModel.created];
    return creationDateString;
}

@end
