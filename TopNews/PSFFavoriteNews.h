//
//  PSFFavoriteNews.h
//  TopNews
//
//  Created by Alexander Kozin on 08.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PSFNewsModel;

@interface PSFFavoriteNews : NSObject

@property (readonly, nonatomic) NSArray<PSFNewsModel *> *newsList;

+ (instancetype)sharedFavoriteNews;
- (void)addToFavorites:(PSFNewsModel *)newsModel;
- (void)removeFromFavorites:(PSFNewsModel *)newsModel;
- (BOOL)containsModel:(PSFNewsModel *)newsModel;
- (void)saveToStorage;

@end
