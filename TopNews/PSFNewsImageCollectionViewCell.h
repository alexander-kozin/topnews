//
//  PSFNewsImageCollectionViewCell.h
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFNewsImageCollectionViewCell : UICollectionViewCell

@property (nonatomic) NSURL *newsImageURL;

@end
