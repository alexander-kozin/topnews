//
//  PSFNewsDetailsViewModel.h
//  TopNews
//
//  Created by Alexander Kozin on 07.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PSFNewsModel;

@interface PSFNewsDetailsViewModel : RVMViewModel

@property (nonatomic) PSFNewsModel *model;
@property (readonly, nonatomic) NSString *creationDateString;

@end
