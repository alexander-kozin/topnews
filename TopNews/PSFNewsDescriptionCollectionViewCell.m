//
//  PSFNewsDescriptionCollectionViewCell.m
//  TopNews
//
//  Created by Alexander Kozin on 07.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsDescriptionCollectionViewCell.h"

@interface PSFNewsDescriptionCollectionViewCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingSpace;

@end

@implementation PSFNewsDescriptionCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    CGFloat edgesInset = self.leadingSpace.constant + self.trailingSpace.constant;
    self.widthConstraint.constant = UIScreen.mainScreen.bounds.size.width - edgesInset;
}

@end
