//
//  PSFFavoritesNewsTableViewController.h
//  TopNews
//
//  Created by Alexander Kozin on 08.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFFavoritesNewsTableViewController : UITableViewController

@end
