//
//  PSFFavoriteNewsViewModel.h
//  TopNews
//
//  Created by Alexander Kozin on 08.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSFFavoriteNewsViewModel : NSObject

@property (nonatomic, readonly) NSArray *favoriteNewsList;

- (NSString *)creationDateStringForModelAtIndex:(NSInteger)index;

@end
