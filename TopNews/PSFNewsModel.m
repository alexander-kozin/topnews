//
//  NewsModel.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsModel.h"

@implementation PSFNewsModel

static NSString * const kTitleArchiveKey = @"title";
static NSString * const kCreatedArchiveKey = @"created";
static NSString * const kFullDescriptionArchiveKey = @"fullDescription";
static NSString * const kImageURLArchiveKey = @"imageURL";

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super init] ) {
        _title = [aDecoder decodeObjectForKey:kTitleArchiveKey];
        _created = [aDecoder decodeObjectForKey:kCreatedArchiveKey];
        _fullDescription = [aDecoder decodeObjectForKey:kFullDescriptionArchiveKey];
        _imageURL = [aDecoder decodeObjectForKey:kImageURLArchiveKey];
        // если новость загружается из архива избранных, то она уже по-умолчанию "избранная"
        _favorite = YES;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:_title forKey:kTitleArchiveKey];
    [aCoder encodeObject:_created forKey:kCreatedArchiveKey];
    [aCoder encodeObject:_fullDescription forKey:kFullDescriptionArchiveKey];
    [aCoder encodeObject:_imageURL forKey:kImageURLArchiveKey];
}

- (BOOL)isEqual:(id)object {
    if ( ![object isKindOfClass:[PSFNewsModel class]] ) {
        return [super isEqual:object];
    }
    PSFNewsModel *comparingModel = (PSFNewsModel *)object;
    if ( [self.title isEqualToString:comparingModel.title] ) {
        return YES;
    }
    return NO;
}

- (NSUInteger)hash {
    return [_title hash];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@> %@", NSStringFromClass([self class]), _title];
}

@end
