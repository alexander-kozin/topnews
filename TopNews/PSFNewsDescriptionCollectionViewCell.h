//
//  PSFNewsDescriptionCollectionViewCell.h
//  TopNews
//
//  Created by Alexander Kozin on 07.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSFNewsDescriptionCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
