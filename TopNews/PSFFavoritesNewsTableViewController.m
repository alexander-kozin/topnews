//
//  PSFFavoritesNewsTableViewController.m
//  TopNews
//
//  Created by Alexander Kozin on 08.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFFavoritesNewsTableViewController.h"
#import "PSFFavoriteNewsViewModel.h"
#import "PSFNewsTableViewCell.h"
#import "PSFNewsModel.h"
#import "PSFNewsDetailsViewModel.h"
#import "PSFNewsDetailsViewController.h"

@interface PSFFavoritesNewsTableViewController ()

@property (nonatomic) PSFFavoriteNewsViewModel *viewModel;

@end

@implementation PSFFavoritesNewsTableViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        _viewModel = [[PSFFavoriteNewsViewModel alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
        
    @weakify(self);
    [RACObserve(self.viewModel, favoriteNewsList) subscribeNext:^(id x) {
        @strongify(self);
        [self.tableView reloadData];
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.favoriteNewsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *kCellId = @"NewsTableViewCell";

    PSFNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellId forIndexPath:indexPath];
    
    PSFNewsModel *newsModel = self.viewModel.favoriteNewsList[indexPath.row];
    cell.titleLabel.text = newsModel.title;
    cell.creationDateLabel.text = [self.viewModel creationDateStringForModelAtIndex:indexPath.row];
    cell.newsModel = newsModel;
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *selectedRowIndexPath = [self.tableView indexPathForSelectedRow];
    if ( selectedRowIndexPath ) {
        PSFNewsModel *newsModel = _viewModel.favoriteNewsList[selectedRowIndexPath.row];
        PSFNewsDetailsViewModel *detailViewModel = [[PSFNewsDetailsViewModel alloc] initWithModel:newsModel];
        PSFNewsDetailsViewController *detailsVC = [segue destinationViewController];
        detailsVC.viewModel = detailViewModel;
    } else {
        NSLog(@"Selected row is missing");
        abort();
    }
}

@end
