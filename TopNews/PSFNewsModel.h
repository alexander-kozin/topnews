//
//  NewsModel.h
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSFNewsModel : NSObject <NSCoding>

@property (nonatomic) NSString *title;
@property (nonatomic) NSDate *created;
@property (nonatomic) NSString *fullDescription;
@property (nonatomic) NSURL *imageURL;
@property (nonatomic, getter=isFavorite) BOOL favorite;

@end
