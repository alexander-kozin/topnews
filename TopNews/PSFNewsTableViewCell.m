//
//  PSFNewsTableViewCell.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsTableViewCell.h"
#import "PSFFavoriteControl.h"
#import "PSFFavoriteNews.h"
#import "PSFNewsModel.h"

@interface PSFNewsTableViewCell () {
    PSFFavoriteNews *favoriteNews;
}

@end

@implementation PSFNewsTableViewCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        favoriteNews = [PSFFavoriteNews sharedFavoriteNews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[RACObserve(self, newsModel) ignore:nil] subscribeNext:^(PSFNewsModel *model) {
        if ( model.favorite ) {
            self.favoriteControl.selected = YES;
        } else {
            self.favoriteControl.selected = NO;
        }
    }];
}

- (void)prepareForReuse {
    self.titleLabel.text = @"";
    self.creationDateLabel.text = @"";
    self.favoriteControl.selected = NO;
}

- (IBAction)favoriteControlWasClicked:(PSFFavoriteControl *)sender {
    if ( self.favoriteControl.isSelected ) {
        sender.selected = NO;
        [favoriteNews removeFromFavorites:self.newsModel];
    } else {
        sender.selected = YES;
        [favoriteNews addToFavorites:self.newsModel];
    }
}

@end
