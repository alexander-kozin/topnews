//
//  PSFNewsTableViewCell.h
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PSFFavoriteControl;
@class PSFNewsModel;

@interface PSFNewsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *creationDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet PSFFavoriteControl *favoriteControl;
@property (nonatomic) PSFNewsModel *newsModel;

- (IBAction)favoriteControlWasClicked:(PSFFavoriteControl *)sender;

@end
