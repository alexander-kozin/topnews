//
//  PSFSettings.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFSettings.h"

@interface PSFSettings() {
    NSDictionary *settingsDictionary;
}

@property (readonly, nonatomic) NSURL *newsBaseURL;
@property (readonly, nonatomic) NSString *newsSource;
@property (readonly, nonatomic) NSString *newsApiKey;

@end

@implementation PSFSettings

+ (instancetype)sharedSettings {
    static PSFSettings *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [PSFSettings new];
    });
    return sharedInstance;
}

- (instancetype)init {
    if ( self = [super init] ) {
        NSBundle *mainBundle = [NSBundle mainBundle];
        NSString *settingsFileName = [mainBundle pathForResource:@"Settings" ofType:@"plist"];
        settingsDictionary = [NSDictionary dictionaryWithContentsOfFile:settingsFileName];
    }
    return self;
}

- (NSURL *)newsURL {
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:self.newsBaseURL resolvingAgainstBaseURL:NO];
    urlComponents.queryItems = @[[NSURLQueryItem queryItemWithName:@"source" value:self.newsSource],
                                 [NSURLQueryItem queryItemWithName:@"apiKey" value:self.newsApiKey]
                                 ];
    return urlComponents.URL;
}

- (NSString *)favoriteNewsStorageName {
    return [settingsDictionary objectForKey:@"favoriteNewsStorageName"];
}

#pragma mark - Private properties

- (NSURL *)newsBaseURL {
    return [NSURL URLWithString:[settingsDictionary objectForKey:@"newsBaseURL"]];
}

- (NSString *)newsSource {
    return [settingsDictionary objectForKey:@"newsSource"];
}

- (NSString *)newsApiKey {
    return [settingsDictionary objectForKey:@"newsApiKey"];
}

@end
