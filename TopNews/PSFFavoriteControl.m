//
//  PSFFavoriteControl.m
//  TopNews
//
//  Created by Alexander Kozin on 08.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFFavoriteControl.h"

@interface PSFFavoriteControl () {
    UIImageView *selectedImageView;
    UIImageView *deselectedImageView;
}

@end

@implementation PSFFavoriteControl

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        UIImage *favoriteImage = [UIImage imageNamed:@"favorite"];
        deselectedImageView = [[UIImageView alloc] initWithImage:favoriteImage];
        deselectedImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:deselectedImageView];
        
        UIImage *selectedFavoriteImage = [UIImage imageNamed:@"favorite_selected"];
        selectedImageView = [[UIImageView alloc] initWithImage:selectedFavoriteImage];
        selectedImageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}

- (void)layoutSubviews {
    @weakify(self);
    [self.subviews enumerateObjectsUsingBlock:^(UIView *view, NSUInteger index, BOOL *stop) {
        @strongify(self);
        view.bounds = self.bounds;
    }];
}

- (void)setSelected:(BOOL)selected {
    if ( self.selected == selected ) {
        return;
    }
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    if ( selected ) {
        [self addSubview:selectedImageView];
    } else {
        [self addSubview:deselectedImageView];
    }
//    [self exchangeSubviewAtIndex:0 withSubviewAtIndex:1];
    [super setSelected:selected];
}

@end
