//
//  PSFNewsListViewModel.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsListViewModel.h"
#import "PSFNewsDownloader.h"
#import "PSFNewsModel.h"
#import "PSFFavoriteNews.h"

@interface PSFNewsListViewModel () {
    PSFFavoriteNews *favoriteNews;
}

@property (nonatomic, getter=isLoading) BOOL loading;

@end

@implementation PSFNewsListViewModel
@dynamic model;

- (instancetype)init {
    if ( self = [super init] ) {
        favoriteNews = [PSFFavoriteNews sharedFavoriteNews];

        @weakify(self);
        [self.didBecomeActiveSignal subscribeNext:^(id x) {
            @strongify(self);
            self.loading = YES;

            RAC(self, model) = [[[PSFNewsDownloader fetchNews] map:^id(NSArray *news) {
                // помечаю избранные новости
                return [[[news rac_sequence] map:^id(PSFNewsModel *newsModel) {
                    if ( [favoriteNews containsModel:newsModel] ) {
                        newsModel.favorite = YES;
                    }
                    return newsModel;
                }] array];
            }] doCompleted:^{
                self.loading = NO;
            }];
        }];
        // обновление списка всех новостей в момент добавления/удаления из избранных новостей
        [RACObserve(favoriteNews, newsList) subscribeNext:^(id x) {
            [self willChangeValueForKey:@"model"];
            
            BOOL anyModelWasChanged = NO;
            for ( PSFNewsModel *newsModel in self.model ) {
                BOOL modelInFavorites = [favoriteNews containsModel:newsModel];

                if ( modelInFavorites != newsModel.favorite ) {
                    newsModel.favorite = !newsModel.favorite;
                    anyModelWasChanged = YES;
                }
            }
            
            if ( anyModelWasChanged ) {
                [self didChangeValueForKey:@"model"];
            }
        }];
    }
    return self;
}

- (NSString *)creationDateStringForModelAtIndex:(NSInteger)index {
    if ( index < 0 || index > self.model.count ) {
        return @"";
    }
    PSFNewsModel *newsModel = self.model[index];
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateStyle = NSDateFormatterNoStyle;
    df.timeStyle = NSDateFormatterShortStyle;
    NSString *creationDateString = [df stringFromDate:newsModel.created];
    return creationDateString;
}

@end
