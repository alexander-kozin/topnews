//
//  PSFNewsTitleCollectionViewCell.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsTitleCollectionViewCell.h"
#import "PSFFavoriteControl.h"
#import "PSFFavoriteNews.h"
#import "PSFNewsModel.h"

@interface PSFNewsTitleCollectionViewCell () {
    PSFFavoriteNews *favoriteNews;
}

@property (weak, nonatomic) IBOutlet PSFFavoriteControl *favoriteControl;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTrailingSpace;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *creationDateLabelLeadingSpace;

- (IBAction)favoriteControlWasClicked:(PSFFavoriteControl *)sender;

@end

@implementation PSFNewsTitleCollectionViewCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ( self = [super initWithCoder:aDecoder] ) {
        favoriteNews = [PSFFavoriteNews sharedFavoriteNews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    CGFloat edgesInsets = self.creationDateLabelLeadingSpace.constant + self.titleLabelTrailingSpace.constant;
    self.titleLabelWidthConstraint.constant = UIScreen.mainScreen.bounds.size.width - edgesInsets;
    
    [RACObserve(self, newsModel.favorite) subscribeNext:^(NSNumber *favoriteValue) {
        if ( favoriteValue.boolValue ) {
            self.favoriteControl.selected = YES;
        } else {
            self.favoriteControl.selected = NO;
        }
    }];
}

- (IBAction)favoriteControlWasClicked:(PSFFavoriteControl *)sender {
    if ( self.favoriteControl.isSelected ) {
        sender.selected = NO;
        [favoriteNews removeFromFavorites:self.newsModel];
    } else {
        sender.selected = YES;
        [favoriteNews addToFavorites:self.newsModel];
    }
}

@end
