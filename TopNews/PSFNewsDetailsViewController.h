//
//  PSFNewsDetailsViewController.h
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PSFNewsDetailsViewModel;

@interface PSFNewsDetailsViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic) PSFNewsDetailsViewModel *viewModel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
