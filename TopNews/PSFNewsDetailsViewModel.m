//
//  PSFNewsDetailsViewModel.m
//  TopNews
//
//  Created by Alexander Kozin on 07.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsDetailsViewModel.h"
#import "PSFNewsModel.h"

@implementation PSFNewsDetailsViewModel
@dynamic model;

- (NSString *)creationDateString {
    NSDateFormatter *df = [NSDateFormatter new];
    df.dateStyle = NSDateFormatterNoStyle;
    df.timeStyle = NSDateFormatterShortStyle;
    return [df stringFromDate:self.model.created];
}

@end
