//
//  PSFNewsDetailsViewController.m
//  TopNews
//
//  Created by Alexander Kozin on 06.07.17.
//  Copyright © 2017 Pragmatic Software. All rights reserved.
//

#import "PSFNewsDetailsViewController.h"
#import "PSFNewsImageCollectionViewCell.h"
#import "PSFNewsTitleCollectionViewCell.h"
#import "PSFNewsDescriptionCollectionViewCell.h"
#import "PSFNewsDetailsViewModel.h"
#import "PSFNewsModel.h"

@interface PSFNewsDetailsViewController ()

@end

@implementation PSFNewsDetailsViewController

static NSString * const kImageCellId = @"PSFNewsImageCollectionViewCell";
static NSString * const kTitleCellId = @"PSFNewsTitleCollectionViewCell";
static NSString * const kDescriptionCellId = @"PSFNewsDescriptionCollectionViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    // для автоматического изменения высоты ячеек в зависимости от размера их содержимого
    flowLayout.estimatedItemSize = CGSizeMake(1, 1);
    
    // регистрация nib-файлов для использования в качестве ячеек
    UINib *imageCellNib = [UINib nibWithNibName:kImageCellId bundle:nil];
    [self.collectionView registerNib:imageCellNib forCellWithReuseIdentifier:kImageCellId];
    UINib *titleCellNib = [UINib nibWithNibName:kTitleCellId bundle:nil];
    [self.collectionView registerNib:titleCellNib forCellWithReuseIdentifier:kTitleCellId];
    UINib *descriptionCellNib = [UINib nibWithNibName:kDescriptionCellId bundle:nil];
    [self.collectionView registerNib:descriptionCellNib forCellWithReuseIdentifier:kDescriptionCellId];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection view data source 

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell;
    PSFNewsImageCollectionViewCell *imageCell;
    PSFNewsTitleCollectionViewCell *titleCell;
    PSFNewsDescriptionCollectionViewCell *descriptionCell;
    switch ( indexPath.item ) {
        case 0:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kImageCellId forIndexPath:indexPath];
            imageCell = (PSFNewsImageCollectionViewCell *)cell;
            imageCell.newsImageURL = self.viewModel.model.imageURL;
            break;
        case 1:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kTitleCellId forIndexPath:indexPath];
            titleCell = (PSFNewsTitleCollectionViewCell *)cell;
            titleCell.titleLabel.text = self.viewModel.model.title;
            titleCell.creationDateLabel.text = self.viewModel.creationDateString;
            titleCell.newsModel = self.viewModel.model;
            break;
        case 2:
            cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDescriptionCellId forIndexPath:indexPath];
            descriptionCell = (PSFNewsDescriptionCollectionViewCell *)cell;
            descriptionCell.descriptionLabel.text = self.viewModel.model.fullDescription;
            break;
        default:
            NSLog(@"Incorrect cell id: %ld", indexPath.item);
            abort();
            break;
    }
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
