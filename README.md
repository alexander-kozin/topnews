# README #

Application loads latest news using API at url "https://newsapi.org/".
### Features: ###
* Display news list.
* Display detailed news.
* Add news to favorites.
* Save favorite news to disk.
* Using ReactiveCocoa.
* Using MVVM template.